import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Rx';
import { User } from './user';
import { Sims } from './sim';
import { Api } from './api';


@Injectable()
export class DataService {

  result:any;
  res: Sims[];

  constructor(private _http: Http) { }

  getUsers() {
    return this._http.get("/api/users")
      .map(result => this.result = result.json().data);
  }

  // updateUsers(users: User) : Observable<any> {
  //   return this._http.put("api/user/:id").pipe(
  //     tap(_this.log('updated user id=$(users.id')),
  //     catchError(this.handleError<any))('updateUser'));
  //   );
  // }

  getFixedUser(){
    return this._http.get("/api/users/fixed")
      .map(result => this.result = result.json().data);
  }
  getMobileUser(){
    return this._http.get("/api/users/mobile")
      .map(result => this.result = result.json().data);
  }

  getMixedUser(){
    return this._http.get("/api/users/mixed")
    .map(result=>this.result=result.json().data)
  }

  getFixedActiveUser(){
    return this._http.get("/api/users/fixed/active")
    .map(result=>this.result = result.json().data);
  }


  getAddress(){
    return this._http.get("api/address")
    .map(result=>this.result = result.json().data);
  }

  getApiData(){
    return this._http.get("api/serviceMonitor")
    .map(result=>this.result = result.json().data);
  }


  getApiStats2(): Promise<void | Api[]> {
    return this._http.get("api/apiStats")
    .toPromise()
    .then(response=>response.json() as Api[])
    .catch(this.handleError);
    // .map(result=>this.sims = result.json().data);
  }

  // getApiStats3(): Api[] {
  //   return this._http.get("api/apiStats").map(result => result.json() as Api[])
  //   // .map(result=>this.result = result.json().data);   
  //   // return this._http.get("api/apiStats")
  //   // .map(result=>this.result = result.json().data);        
  // }



  getSims(): Promise<void | Sims[]> {
    return this._http.get("api/getSim")
    .toPromise()
    .then(response=>response.json() as Sims[])
    .catch(this.handleError);;
    // .map(result=>this.sims = result.json().data);
  }
  fetchSim() {
    return this._http.get("api/getSim")
    .map(result=>this.result = result.json().data);
  }
  unUsedSim() {
    return this._http.get("api/availableSim")
    .map(result=>this.result = result.json().data);
  }

  showSim(id){
    return this._http.get("api/getSim/"+id)
    .map(result=>this.result = result.json().data);
  }

  updateSim(id, data){
    console.log("data going is "+ JSON.stringify(data));
    return this._http.put("api/updateSim/"+id,data);
    // .map(result=>this.res = result.json().data);
  }

  saveUserComment(id, data){
    console.log("data going is " + JSON.stringify(data));
    return this._http.put("api/updateUser/"+id, data);
  }

  getApiStats() {
    return this._http.get("api/apiStats")
    .map(result=>this.result = result.json().data);        
  }
  
  getErroredAPI(environment){
    return this._http.get("api/getErrorred",environment)
    .map(result=>this.result = result.json().data);        
  }
  getErroredAPIAll(){
    return this._http.get("api/getErroredAll")
    .map(result=>this.result = result.json().data);        
  }

  // putSim(id, putSim: Sims): Promise<void | Sims>{
  //   return this._http.put("api/updateSim/"+id, putSim)
  //   .toPromise()
  //   .then(response=>response.json() as Sims)
  //   .catch(this.handleError);

  // }

  private handleError (error: any) {
      let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      console.error(errMsg); // log to console instead
    }


}