import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonTemplatesComponent } from './common-templates.component';

describe('CommonTemplatesComponent', () => {
  let component: CommonTemplatesComponent;
  let fixture: ComponentFixture<CommonTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
