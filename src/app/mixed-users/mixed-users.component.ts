import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';


@Component({
  selector: 'app-mixed-users',
  templateUrl: './mixed-users.component.html',
  // styleUrls: ['./mixed-users.component.css']
  styleUrls: ['../common/syles.css']
  
})
export class MixedUsersComponent implements OnInit {

  users:Array<any>;
  constructor(private _dataService: DataService) { 
    this._dataService.getMixedUser()
    .subscribe(res=>this.users=res);
  }

  ngOnInit() {
  }

  
}
