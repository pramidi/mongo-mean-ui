import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixedUsersComponent } from './mixed-users.component';

describe('MixedUsersComponent', () => {
  let component: MixedUsersComponent;
  let fixture: ComponentFixture<MixedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
