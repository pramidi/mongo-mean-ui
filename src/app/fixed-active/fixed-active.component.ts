import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-fixed-active',
  templateUrl: './fixed-active.component.html',
  // styleUrls: ['./fixed-active.component.css']
  styleUrls: ['../common/syles.css']
})
export class FixedActiveComponent implements OnInit {

  users: Array<any>;

  constructor(private _dataService: DataService) { 
    this._dataService.getFixedActiveUser()
    .subscribe(res=>this.users=res);
  }

  ngOnInit() {
  }

}
