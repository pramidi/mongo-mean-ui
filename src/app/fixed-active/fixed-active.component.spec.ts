import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedActiveComponent } from './fixed-active.component';

describe('FixedActiveComponent', () => {
  let component: FixedActiveComponent;
  let fixture: ComponentFixture<FixedActiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedActiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedActiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
