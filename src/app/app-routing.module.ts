import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AllUsersComponent } from './all-users/all-users.component';
import {AddressComponent} from './address/address.component';
import { DashboardComponent }from './dashboard/dashboard.component';
import {HomeComponent} from './home/home.component';
import { MobileUsersComponent} from './mobile-users/mobile-users.component';
import { FixedUsersComponent} from './fixed-users/fixed-users.component';
import { MixedUsersComponent }from './mixed-users/mixed-users.component';
import{ FixedActiveComponent} from './fixed-active/fixed-active.component';
import{ TestSimsComponent} from './test-sims/test-sims.component';
import{ UnusedSimComponent} from './unused-sim/unused-sim.component';
import { ServiceMonitorComponent } from './service-monitor/service-monitor.component';
import { ErrorApiComponent } from './error-api/error-api.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'allUsers', component: AllUsersComponent},
  {path: 'users', component: AllUsersComponent},
  {path: 'address', component: AddressComponent},
  {path: 'home', component: HomeComponent},
  {path: 'mobile',component: MobileUsersComponent},
  {path: 'fixed', component: FixedUsersComponent},
  {path: 'mixed', component:MixedUsersComponent},
  {path: 'fixed-active', component: FixedActiveComponent},
  {path: 'testSim', component:TestSimsComponent},
  {path: 'availableSim', component:UnusedSimComponent},
  {path: 'serviceMonitor', component:ServiceMonitorComponent},
  {path: 'errorApi', component:ErrorApiComponent}

]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})



export class AppRoutingModule { }
