import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  // styleUrls: ['./all-users.component.css']
  styleUrls: ['../common/syles.css']
})
export class AllUsersComponent implements OnInit {

// Define a users property to hold our user data
  users: {};

  // Create an instance of the DataService through dependency injection
  constructor(private _dataService: DataService) {

    // Access the Data Service's getUsers() method we defined
    this._dataService.getUsers()
        .subscribe(res => this.users = res);
  }

  ngOnInit() {
  }

}
