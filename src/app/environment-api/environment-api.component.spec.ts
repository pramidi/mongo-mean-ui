import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvironmentApiComponent } from './environment-api.component';

describe('EnvironmentApiComponent', () => {
  let component: EnvironmentApiComponent;
  let fixture: ComponentFixture<EnvironmentApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvironmentApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvironmentApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
