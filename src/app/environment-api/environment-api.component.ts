import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { DataService } from './../data.service';
import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { Api } from '../api';

@Component({
  selector: 'app-environment-api',
  templateUrl: './environment-api.component.html',
  styleUrls: ['../common/syles.css']
})
export class EnvironmentApiComponent implements OnInit {

  // api: Array<any>;
  dataSource: Api[];
  selectedApi: Api;

  constructor(private _dataService: DataService) { 
    this._dataService.getApiStats()
    .subscribe(res=>this.dataSource=res)

    // this._dataService.getApiStats()
    // .subscribe(res=>this.dataSource=res);
     
    // this._dataService
    // .getApiStats3()
    // .then((api: Api[]) => {
    //   this.dataSource = api.map((api) => {
    //     return this.dataSource;
    //   });
    // });
  }
    
    
  customizeTootip(arg: any){
    return {
      text: arg.valueText
     };
  }

  ngOnInit() {
  }

  selectApi(api: Api){
    this.selectedApi = api;
    return this.selectedApi;
  }

  getErroredAPI(api){
    var doc = this.selectApi(api);  
    this._dataService.getErroredAPI(doc)
    .subscribe(res=>api=res);
  }

}
