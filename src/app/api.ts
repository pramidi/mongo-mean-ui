
export class Api {
    _id: String;
    environment: String;
    apiTotal: number;
    apiSuccess: number;
    apiErrored: number;
    updatedTime: String;
    responseTime: String;
    responseStatus: String;
    serviceURL: String;
    serviceName: String;
    
  }
  