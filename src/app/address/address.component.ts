import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  // styleUrls: ['./address.component.css']
  styleUrls: ['../common/syles.css']
})
export class AddressComponent implements OnInit {

address: Array<any>;
  constructor(private _dataService: DataService) { 
    this._dataService.getAddress()
        .subscribe(res => this.address = res);
  }

  ngOnInit() {
  }

}
