import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedUsersComponent } from './fixed-users.component';

describe('FixedUsersComponent', () => {
  let component: FixedUsersComponent;
  let fixture: ComponentFixture<FixedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
