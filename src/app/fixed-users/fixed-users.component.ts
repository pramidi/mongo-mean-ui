import { Comment } from '@angular/compiler';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';



@Component({
  selector: 'app-fixed-users',
  // templateUrl: './fixed-users.component.html',
  
  // styleUrls: ['./fixed-users.component.css']
  templateUrl: '../common/fixedUsers.html',
  styleUrls: ['../common/syles.css']

})
export class FixedUsersComponent implements OnInit {

  users: User[];
  selectedUser: User;
  constructor(private _dataService: DataService) {
    this._dataService.getFixedUser()
    .subscribe(res=>this.users = res);
   }

  //  add(name: String ): void{
  //    name=name.trim();
  //    if(!name){return;}
  //    this.heroServices.addHero({ name } as Hero).subscribe(hero=>{
  //      this.hero.push(hero);
  //    })
  //  }

  ngOnInit() {
  }

  private getIndexOfUser = (userId: String) => {
    return this.users.findIndex((user) => {
      return user._id === userId;
    });
  }

  selectUser(user: User){
    this.selectedUser = user;
  }

  updateUser = (user: User) => {
    var idx = this.getIndexOfUser(user._id);
    if(idx !== -1){
      this.users[idx] = user;
      this.selectUser(user);
    }
    return this.selectedUser;
  }

  saveComment(comment, s){
    var doc = this.updateUser(s);
    doc.comment = doc.comment+comment;
  }

  saveUser(s,comment){
    var doc = this.updateUser(s);
    doc.comment= comment;
    var id = doc._id;

    this._dataService.saveUserComment(id,doc).subscribe(
      res => {
        s = res;
      }
    )
  }
}
