import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { Sims } from '../sim'

@Component({
  selector: 'app-unused-sim',
  templateUrl: './unused-sim.component.html',
  // styleUrls: ['./unused-sim.component.css']
  styleUrls: ['../common/syles.css']
})
export class UnusedSimComponent implements OnInit {

  sims: Sims[];
  selectedSim: Sims;

   constructor(private _dataService: DataService, private router:Router) { 
    this._dataService.unUsedSim().subscribe(res=>this.sims=res);
  }

  ngOnInit() {
  }
  private getIndexOfSim = (simId: String) => {
    return this.sims.findIndex((sim)=>{
      return sim._id === simId;
    });
  }

  notUsed(sim:Sims){
    
    try{
      if(sim.used == "USED"){
      return true;
      

    }else{
      return false;
      
    }

  }catch(e) {
    console.log(e.stringify());
        return false;
    }
    
  }


  selectSim(sim:Sims){
    this.selectedSim = sim;
    this.selectedSim.used = "USED";
    var d=new Date();
    this.selectedSim.updatedTime = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes();
  }

  updateTestSim = (sim: Sims) => {
      var idx = this.getIndexOfSim(sim._id);
      if(idx !== -1){
        this.sims[idx] =sim;
        this.selectSim(sim);
      }
      return this.selectedSim;
    }
  editSim(s){
    var doc = this.updateTestSim(s);
    var id = doc._id;
    
    // doc.comment=(<HTMLInputElement>document.getElementById("comment")).value;
    this._dataService.updateSim(id,doc).subscribe(res=> {
      s=res;
      var bid = doc.simNo;
      document.getElementById(bid).remove();
      // document.location.reload();
    });
    
  }


}
