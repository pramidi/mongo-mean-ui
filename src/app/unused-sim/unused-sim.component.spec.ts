import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnusedSimComponent } from './unused-sim.component';

describe('UnusedSimComponent', () => {
  let component: UnusedSimComponent;
  let fixture: ComponentFixture<UnusedSimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnusedSimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnusedSimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
