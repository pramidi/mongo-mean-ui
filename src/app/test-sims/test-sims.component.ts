import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { Sims } from '../sim'

@Component({
  selector: 'app-test-sims',
  templateUrl: './test-sims.component.html',
  // styleUrls: ['./test-sims.component.css']
  styleUrls: ['../common/syles.css']
})
export class TestSimsComponent implements OnInit {

  sims: Sims[];
  selectedSim: Sims;

  constructor(private _dataService: DataService) { 
    this._dataService.fetchSim().subscribe(res=>this.sims=res);
  }

  ngOnInit() {
    // this._dataService
    // .getSims()
    // .then((sims: Sims[]) => {
    //   this.sims = sims.map((sim) => {
    //     return sim;
    //   });
    // });
  }

  notUsed(sim:Sims){
    
    try{
      if(sim.used == "USED"){
      return true;
      

    }else{
      return false;
      
    }

  }catch(e) {
    console.log(e.stringify());
        return false;
    }
    
  }

  private getIndexOfSim = (simId: String) => {
    return this.sims.findIndex((sim)=>{
      return sim._id === simId;
    });
  }

  selectSim(sim:Sims){
    this.selectedSim = sim;
    this.selectedSim.used = "USED";
    var d = new Date();
    this.selectedSim.updatedTime = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes();
  }

  updateTestSim = (sim: Sims) => {
    var idx = this.getIndexOfSim(sim._id);
    if(idx !== -1){
      this.sims[idx] =sim;
      this.selectSim(sim);
    }
    return this.selectedSim;
  }
  saveComment(comment, s){
    var doc = this.updateTestSim(s);
    doc.comment= doc.comment+comment;
  }

  editSim(s){
    var doc = this.updateTestSim(s);
    var id = doc._id;
    
    // doc.comment=(<HTMLInputElement>document.getElementById("comment")).value;
    this._dataService.updateSim(id,doc).subscribe(res=> {
      s=res;
      var bid = doc.simNo;
      document.getElementById(bid).remove();
      // document.location.reload();
    });
    
  }

}
