import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSimsComponent } from './test-sims.component';

describe('TestSimsComponent', () => {
  let component: TestSimsComponent;
  let fixture: ComponentFixture<TestSimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSimsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
