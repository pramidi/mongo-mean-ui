import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-mobile-users',
  templateUrl: './mobile-users.component.html',
  // styleUrls: ['./mobile-users.component.css']
  styleUrls: ['../common/syles.css']
})
export class MobileUsersComponent implements OnInit {

  users: Array<any>;
  constructor(private _dataService: DataService) {
    this._dataService.getMobileUser()
    .subscribe(res=>this.users = res);
   }

  ngOnInit() {
  }

}
