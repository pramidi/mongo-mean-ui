export class Sims {
  _id: string;
  simNo: string;
  used: string;
  environment: string;
  comment: string;
  status:string;
  octane_status: String;
  createdTime: String;
  updatedTime: String;
  
}
