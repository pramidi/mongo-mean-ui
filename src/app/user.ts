
export class User {
  _id: String;
  environment: String;
  comment: string;
  customerEmail: String;
  password: String;
  utilBillID: String;
  active: String;
  mobileUser: String;
  fixedUser:String;
  createdTime: String;
  connectionType: String;
  SERVICE_CLASS: String;
  paymentType: String;
  dataPlan: String;
}
