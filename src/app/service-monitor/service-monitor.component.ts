import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';
import { Api } from "../api";

@Component({
  selector: 'app-service-monitor',
  templateUrl: './service-monitor.component.html',
  // styleUrls: ['./service-monitor.component.css']
  styleUrls: ['../common/syles.css']
})
export class ServiceMonitorComponent implements OnInit {

  api: Api[];

  constructor(private _dataService: DataService) {
    this._dataService.getErroredAPIAll()
    .subscribe(res=>this.api=res);
   }



  ngOnInit() {
  }

}
