import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

// Import the Http Module and our Data Service
import { HttpModule } from '@angular/http';
import { DataService } from './data.service';
import { AllUsersComponent } from './all-users/all-users.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddressComponent } from './address/address.component';
import { HomeComponent } from './home/home.component';
import { FixedUsersComponent } from './fixed-users/fixed-users.component';
import { MobileUsersComponent } from './mobile-users/mobile-users.component';
import { MixedUsersComponent } from './mixed-users/mixed-users.component';
import { FixedActiveComponent } from './fixed-active/fixed-active.component';
import { TestSimsComponent } from './test-sims/test-sims.component';
import { UnusedSimComponent } from './unused-sim/unused-sim.component';
import { CommonTemplatesComponent } from './common-templates/common-templates.component';
import { ServiceMonitorComponent } from './service-monitor/service-monitor.component';
import { EnvironmentApiComponent } from './environment-api/environment-api.component';
import { DxChartModule } from 'devextreme-angular';
import { ErrorApiComponent } from './error-api/error-api.component';

@NgModule({
  declarations: [
    AppComponent,
    AllUsersComponent,
    DashboardComponent,
    AddressComponent,
    HomeComponent,
    FixedUsersComponent,
    MobileUsersComponent,
    MixedUsersComponent,
    FixedActiveComponent,
    TestSimsComponent,
    UnusedSimComponent,
    CommonTemplatesComponent,
    ServiceMonitorComponent,
    EnvironmentApiComponent,
    ErrorApiComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    DxChartModule,
    AppRoutingModule              // <-Add HttpModule
  ],
  providers: [DataService], // <-Add DataService
  bootstrap: [AppComponent]
})
export class AppModule { }