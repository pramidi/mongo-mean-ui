const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

// Connect
const connection = (closure) => {
    return MongoClient.connect('mongodb://10.0.3.238:27017/local', (err, db) => {
    // return MongoClient.connect('mongodb://localhost:27017/mean', (err, db) => {
        if (err) return console.log(err);
        closure(db);
    });
};

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

// Get users
router.get('/users', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find({})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.put('/users/:id',(req,res) => {
    connection((db) => {
        db.collection('users')
        .updateOne({_id: new ObjectID(req.params.id)}, req.body, function(err,result){
            if(err){
                sendError(err,res);
            }else{
                res.status(204).end();
            }
        })

    });
})

router.get('/users/fixed', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find({"mobileUser":{"$ne":"yes"},
        "fixedUser":"yes"})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});
router.get('/users/fixed/active', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find({"mobileUser":{"$ne":"yes"}, 
            "fixedUser":"yes","active":"true"})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.get('/users/mixed', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find({"mobileUser":"yes","fixedUser":"yes"})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.get('/address', (req, res) => {
    connection((db) => {
        db.collection('PendingOrderPreProd')
            .find({})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.get('/users/mobile', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find({"mobileUser":"yes"})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.get('/availableSim', (req, res) => {
    connection((db) => {
        db.collection('testSims')
            .find({"used":{"$ne":"USED"}})
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.get('/getSim', (req, res) => {
    connection((db) => {
        db.collection('testSims')
            .find({"used":{"$ne":"yes"}})
            .toArray()
            .then((data) => {
                response.data = data;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});
//show sim
router.get('/getSim/:id', (req, res,next) => {
    connection((db) => {
        db.collection('testSims')
            .findOne(req.params.id, function(err,post){
                if(err) return next(err);
                res.json(post);
            });
    });
});


//update sim by id
router.put('/updateSim/:id', (req, res,next) => {
    var uDoc = req.body;
    delete uDoc._id;
    connection((db) => {
        db.collection('testSims')
            .findOneAndUpdate({_id: new ObjectID(req.params.id)}, uDoc, function(err,post){
                if(err){ 
                    sendError(err,res);
                }else{
                    res.sendStatus(204);
                }
            });
    });
});

router.put('/updateUser/:id', (req, res,next) => {
    var uDoc = req.body;
    delete uDoc._id;
    connection((db) => {
        db.collection('users')
            .findOneAndUpdate({_id: new ObjectID(req.params.id)}, uDoc, function(err,post){
                if(err){ 
                    sendError(err,res);
                }else{
                    res.sendStatus(204);
                }
            });
    });
});

//show api statuses
router.get('/serviceMonitor', (req, res,next) => {
    connection((db) => {
        db.collection('serviceMonitor')
        .find({})
        .toArray()
        .then((users) => {
            response.data = users;
            res.json(response);
        })
        .catch((err) => {
            sendError(err, res);
        });
    });
});

//show api statuses
router.get('/apiStats', (req, res,next) => {
    connection((db) => {
        db.collection('serviceMonitor')
        .find({"stats":"stats"})
        .toArray()
        .then((users) => {
            response.data = users;
            res.json(response);
        })
        .catch((err) => {
            sendError(err, res);
        });
    });
});

router.get('/getErrored',(req, res,next) => {
    var uDoc = req.body;
    delete uDoc._id;
    connection((db) => {
        db.collection('serviceMonitor')
        .find({"environment":req.param.environment,"responseStatus":req.param.responseStatus})
            .find({_id: new ObjectID(req.params.id)}, uDoc, function(err,post){
                if(err){ 
                    sendError(err,res);
                }else{
                    res.sendStatus(204);
                }
            });
    });
});

router.get('/getErroredAll', (req, res,next) => {
    connection((db) => {
        db.collection('serviceMonitor')
        .find({"responseStatus":500})
        .toArray()
        .then((users) => {
            response.data = users;
            res.json(response);
        })
        .catch((err) => {
            sendError(err, res);
        });
    });
});
//

module.exports = router;